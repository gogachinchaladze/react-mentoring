import styled from "styled-components";


const Button = styled.button`
  text-transform: uppercase;
  color: ${props => props.color};
  background-color: ${props => props.bg ? props.bg : "white"};
  cursor: pointer;
  padding: .7em;
  box-shadow: 0 0 0 .15em black;
  transition: box-shadow .3s ease;
  font: bold 1.2em 'Courier New', monospace; 
  
  &:hover {
    box-shadow: 0 0 0 .3em black;
  }
`;

export default Button;

