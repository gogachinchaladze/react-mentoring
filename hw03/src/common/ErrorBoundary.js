const ErrorBoundary = (props) => {
    const errorMessage = <h3 class="error">An error occurred. Please try again later.</h3>;

    let isEverythingOkay = true;

    return <>{ isEverythingOkay ? props.children : errorMessage}</>
};

export default ErrorBoundary;