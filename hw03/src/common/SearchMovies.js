import Button from "./Button";
import PropTypes from "prop-types";
import {useState} from "react";

const ButtonStyles = {
    marginLeft: "1em",
};

const InputStyles = {
    height: "2em",
    backgroundColor: "white",
    border: "3px solid gray",
    width: "16em",
    font: "bold 35px 'Courier New', monospace",
    verticalAlign: "middle",
};

function SearchMovies(props) {
    const [searchText, setSearchText] = useState("");

    const performSearch = () => {
        alert(`Searching for: ${searchText}`);
    }
    return (
        <>
            <input type="text"
                   placeholder={props.placeholder}
                   value={searchText}
                   onChange={e => setSearchText(e.target.value)}
                    style={InputStyles}/>
            <Button style={ButtonStyles}
                    onClick={performSearch}
                    disabled={searchText === ''}>
                Search
            </Button>
        </>
    );
}
SearchMovies.propTypes = {
    placeholder: PropTypes.string,
};

SearchMovies.defaultProps = {
    placeholder: "Type movie name..."
};

export default SearchMovies;