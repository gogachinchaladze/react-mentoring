import Button from "./Button";


function AddMovie() {
    return <Button bg="#555555" color="white"><i>+</i> Add Movie</Button>;
}

export default AddMovie;