import logo from "../../assets/img/logo.png";
import AddMovie from "../common/AddMovie";
import "./header.scss";
import SearchMovies from "../common/SearchMovies";

function Header() {
    return (
        <header>
            <div className="header-first-line">
                <img src={logo} alt="Netflix logo" className="logo"/>
                <AddMovie/>
            </div>
            <h2 className="find-movie-title">Find your movie</h2>
            <SearchMovies placeholder="What do you want to watch?"></SearchMovies>
        </header>
    );
}

export default Header;