import "./reset.scss";
import "./app.scss";
import Header from "./header/Header";
import ErrorBoundary from "./common/ErrorBoundary";


const App = () => {
    return (
        <ErrorBoundary>
            <Header/>
        </ErrorBoundary>
    );
};

export default App;
