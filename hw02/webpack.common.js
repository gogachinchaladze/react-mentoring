const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
    template: "../public/index.html",
    filename: "./index.html"
});

module.exports = {
    context: path.join(__dirname, 'src'),
    entry: "./index.js",
    output: {
        filename: "bundle.js",
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                loader: "babel-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/,
                loader: "sass-loader",
                exclude: /node_modules/,
            },
        ],
    },
    plugins: [htmlPlugin]
};