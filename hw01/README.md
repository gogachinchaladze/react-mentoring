# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Description
Created the repository, initialized the app using create-react-app.

Created components in all 4 ways:
1. React.CreateElement - HelloWorld.js
2. React.Component - Steps.js
3. React.PureComponent - Step.js
4. Functional component - Description.js


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.