import './App.css';
import React from "react";
import Description from "./Description";
import Steps from "./Steps";
import HelloWorld from "./HelloWorld";

function App() {
    return (
        <div className="App">
            {HelloWorld}
            <Description author="Giorgi Chinchaladze"></Description>
            <Steps></Steps>
        </div>
    );
}

export default App;
