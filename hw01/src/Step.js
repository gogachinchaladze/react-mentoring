import React from "react";

class Step extends React.PureComponent {
    constructor(props) {
        super();
    }

    render() {
        return (
            <li>Step #{this.props.index}: {this.props.text}</li>
        );
    }
}

export default Step;

