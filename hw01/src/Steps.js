import React from "react";
import Step from './Step';

class Steps extends React.Component {
    state = {
        steps: ['Core concepts', 'Webpack', 'Components', 'The rest']
    }

    render() {
        return (
            <ul>
                {this.state.steps.map((step, i) => <Step key={step} text={step} index={i+1}/>)}
            </ul>
        );
    }
}

export default Steps;